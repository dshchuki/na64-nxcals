# na64-nxcals

## Description

Helper scripts allowing access to NXCALS database from LXPLUS environment using PyTimber. Made for accessing data from SPS logs which is relevant to NA64 experiment data taking (fixed target intensity, timing, etc.)

## Request read access to NXCALS data

Join [it-hadoop-nxcals-pro-analytics](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10249154) e-group. Aloow for some time to proccess the request.  

## Installation

Using default LXPLUS environment execute:

```
git clone https://gitlab.cern.ch/dshchuki/na64-nxcals.git
cd na64-nxcals
source install-pytimber.sh
```

## Usage 

Setup python virtual environment to work with Pytimber, user will be prompted for password to obtain KerberOS token:

```
source setup-venv.sh
```

Specify time range for extraction by editing `get-log-data.py`:

```
#specify time window for extraction
stime = '2022-08-11 17:41:00'
etime = '2022-08-11 17:42:00'
```

Query the db and generate .csv file with general info about SPS spills inside defined time window. These values are extracted for each SPS spill which has fixed target destination: 
 - date and time of spill start and end with millisecond precision
 - counter
 - effective spill length in milliseconds
 - integrated intensity on T2 target
 - multiplicity for T2 target
 - mean spill intensity in arb. units
 Output file(na64-log-data.csv) is rewritten each time.

```
python get-log-data.py
```

Separate script is used to extract instantaneous intensity vs time for each spill. Specify time range for extraction by editing `get-spill-structure.py`:

```
#specify time window for extraction
stime = '2022-08-11 17:41:00'
etime = '2022-08-11 17:42:00'
```

Query the db and generate .csv file with spill time structure. Extracts high frequency measure of instantaneous intensity of each spill inside defined time window. Produces large .csv files with ~10k entries in each row. Output file(na64-spill-structure.csv.csv) is rewritten each time. Extracted values:
 - date and time of spill start and end with millisecond precision
 - counter
 - spill intesity[arb. units] vs. time

```
python get-spill-structure.py
```

To exit from python virtual environment:

```
deactivate
```

## Following this README

Some resources cited in this repo(marked with 'CERN internal') are inside CERN network. To access from remote location one can use Terminal Services via RDP. More info can be found here: [Login into Terminal Services](https://espace.cern.ch/winservices-help/Terminal%20Services/Introduction/Pages/Login%20into%20Terminal%20Services.aspx)

