#!/bin/sh
#First time setup of PyTimber
#Source: (CERN internal) https://wikis.cern.ch/display/PYT/PyTimber+installation
source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc8-opt/setup.sh
VENV_PATH=$(pwd)/pytimber3-env
unset PYTHONPATH
python -m venv $VENV_PATH
source $VENV_PATH/bin/activate
python -m pip install -U --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch  pytimber
deactivate