import pytimber
import datetime
#select source for log data
logdb = pytimber.LoggingDB(source="nxcals")
#specify time window for extraction
stime = '2022-08-11 17:41:00'
etime = '2022-08-11 17:42:00'
#query the db
print('Starting database query, might take several minutes to execute...')
data = logdb.get(['XTIM.SX.WE-CT:Acquisition:acqC','SPSQC:EFF_SPILL_LENGHT','XTIM.SX.WE-CT:Acquisition:oCounter','SPSQC:BSI'], stime, etime,'SPS:SFT_PRO_MTE_L4780_2022_V1:SFTPRO1')
#parse the response and output to .csv file (overwrites existing file)
tmstmp = (data['XTIM.SX.WE-CT:Acquisition:acqC'][0]).tolist()       #SPS log timestamp
dwrext = (data['XTIM.SX.WE-CT:Acquisition:acqC'][1]).tolist()       #amount of milliseconds from timestamp till extraction start 
splgth = (data['SPSQC:EFF_SPILL_LENGHT'][1]).tolist()               #effective spill length is milliseconds
cntext = (data['XTIM.SX.WE-CT:Acquisition:oCounter'][1]).tolist()   #counter associated with extraction
intsty = (data['SPSQC:BSI'][1]).tolist()                            #high frequency measure of spill intensity in arb. units
with open('na64-spill-structure.csv', 'w') as f:
  _ = f.write("# extracted time window: from "+ stime +" to "+ etime +"\n")
  _ = f.write("# date, start spill, end spill, WE signal counter, spill intensity[a.u.] vector\n")
  for tt, dd, ss, cc, ii in zip(tmstmp,dwrext,splgth,cntext,intsty):
    spillstart = round(tt+dd/1000,2)
    spillend   = round(tt+dd/1000+ss/1000,2)
    _ = f.write("%s" %( datetime.datetime.fromtimestamp(spillstart).date() ))
    _ = f.write(",%s" %( datetime.datetime.fromtimestamp(spillstart).time().isoformat(timespec = 'milliseconds') ))
    _ = f.write(",%s" %( datetime.datetime.fromtimestamp(spillend).time().isoformat(timespec = 'milliseconds') ))
    _ = f.write(",%d" %(cc))
    #average intensity value over groups of 50 to reduce amount of entries (db query returns 480k values for a normal SPS spill!) 
    groups = [ii[ix:ix+49] for ix in range(0, len(ii), 50)]
    means = [sum(group)/len(group) for group in groups]
    #print intensity values
    for iii in means:
      _ = f.write(",%d" %(round(iii)))
    _ = f.write("\n")