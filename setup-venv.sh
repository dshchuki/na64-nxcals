#!/bin/sh
#set -v
source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc8-opt/setup.sh
VENV_PATH=$(pwd)/pytimber3-env
unset PYTHONPATH
python -m venv $VENV_PATH
source $VENV_PATH/bin/activate
echo "Obtain Kerberos token with kinit:"
kinit
#set +v