import pytimber
import datetime
#select source for log data
logdb = pytimber.LoggingDB(source="nxcals")
#specify time window for extraction
stime = '2022-08-11 17:41:00'
etime = '2022-08-11 17:42:00'
#query the db
print('Starting database query, might take several minutes to execute...')
data = logdb.get(['XTIM.SX.WE-CT:Acquisition:acqC','SPSQC:EFF_SPILL_LENGHT','SPS.T2:INTENSITY','SPS.T2:MULTIPLICITY','SPSQC:MEAN_SPILL_INTENSITY','XTIM.SX.WE-CT:Acquisition:oCounter'], stime, etime,'SPS:SFT_PRO_MTE_L4780_2022_V1:SFTPRO1')
#parse the response and output to .csv file (overwrites existing file)
tmstmp = (data['XTIM.SX.WE-CT:Acquisition:acqC'][0]).tolist()       #SPS log timestamp
dwrext = (data['XTIM.SX.WE-CT:Acquisition:acqC'][1]).tolist()       #amount of milliseconds from timestamp till extraction start 
intsty = (data['SPS.T2:INTENSITY'][1]).tolist()                     #SPS T2 intensity value
mtpcty = (data['SPS.T2:MULTIPLICITY'][1]).tolist()                  #SPS T2 multiplicity value
splgth = (data['SPSQC:EFF_SPILL_LENGHT'][1]).tolist()               #effective spill length is milliseconds
aspint = (data['SPSQC:MEAN_SPILL_INTENSITY'][1]).tolist()           #mean spill intensity in arb. units
cntext = (data['XTIM.SX.WE-CT:Acquisition:oCounter'][1]).tolist()   #counter associated with extraction
with open('na64-log-data.csv', 'w') as f:
  #header with general info  
  _ = f.write("# extracted time window: from "+ stime +" to "+ etime +"\n")
  _ = f.write("# date, start spill, end spill, WE signal counter, eff. spill length, T2 intensity, T2 multiplicity, mean spill intensity[a.u.]\n")
  #data
  for tt, dd, ii, mm, ss, aa, cc in zip(tmstmp,dwrext,intsty,mtpcty,splgth,aspint,cntext):
    #calculate actual time of spill start and end
    spillstart = round(tt+dd/1000,2)
    spillend   = round(tt+dd/1000+ss/1000,2)
    _ = f.write("%s" %( datetime.datetime.fromtimestamp(spillstart).date() ))
    _ = f.write(",%s" %( datetime.datetime.fromtimestamp(spillstart).time().isoformat(timespec = 'milliseconds') ))
    _ = f.write(",%s" %( datetime.datetime.fromtimestamp(spillend).time().isoformat(timespec = 'milliseconds') ))
    _ = f.write(",%d" %(cc))
    _ = f.write(",%d" %(ss))
    _ = f.write(",%5.3E" %(ii))
    _ = f.write(",%5.2f" %(mm))
    _ = f.write(",%6.1f" %(aa))
    _ = f.write("\n")
